# Redmine Overtime Calculator

Extracts number of hours spent per month, gets work (business) days for those months and then calculates bonus amounts.

# Dependencies  

* stack
* tool to calculate work days in a month (for Czech work days you can use https://gitlab.com/monnef/czech-work-days-hs)

# Configuration

Copy `config.example.json` to `config.json` and fill appropriately.

As an `extractorCmd` you can use [redmine-extractor](https://gitlab.com/monnef/redmine-extractor). User is passed via template variable `%USER%`. If you use `redmine-extractor`, your `extractorCmd` can look similar to this: `redmine-extractor --user %USER% --url http://redmine.favorlogic.com`.

As a `workDaysInMonthCmd` for Czech work days, you can use [czech-work-days-hs](https://gitlab.com/monnef/czech-work-days-hs) with value of `workDaysInMonthCmd` set to `"czech-work-days-hs %YEAR% %MONTH%"`.

## Database

The databse file is not required.

Copy `db.example.json` to `db.json` and fill your leave days.

Numbers `leave.days` and `sick.days` represent number of off/sick days **before** calculating in `workingDaysPerWeek`. For example: taking `3` days off when `workingDaysPerWeek` = `2.5` (half-time) means you should fill a value `3` (you were expected to work `3` days, each day number of hours relative to your full-time/part-time, meaning full-time `8` hours, half-time `4` hours and so on).

Extra hours in `extra.hours` are hours not included in data from Redmine (e.g. tracked in external system).

Invalid format of DB will be interpreted as empty DB.

Do NOT count leave/sick days on holidays (a record for a specific month with a week of sick leave, when the week has one-day holiday, should have `4` in `days` field).

# Usage

Start via `stack run`.

Results are printed in a console and saved to a file, e.g. `result_2022-11-20_14-46-43.csv`.
