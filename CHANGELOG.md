# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- full month column in console output
- extra hours

### Changed
- result CSV file name now contains date and time when it was generated

### Removed
- unused broken backends and their dependencies

## [0.2.0]

### Added
- sick days

### Changed
- leave days are now assumed to be in "full time"
  - e.g. in old format half-time `1` day becomes now `2` days
  - simply put - each day you are expected to work `n` hours relative to your part-time employment (full-time is `8` hours per day, half-time is `4` hours per day and so on)
