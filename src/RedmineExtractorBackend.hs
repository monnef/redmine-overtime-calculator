{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecordWildCards #-}

module RedmineExtractorBackend (getDataFromBrowser) where

import Constants
import Data
import Data.Aeson (eitherDecode)
import Data.Either.Combinators (mapLeft)
import Data.Either.Utils (forceEitherMsg)
import qualified Data.Text as T
import System.Exit (ExitCode (ExitSuccess), exitFailure)
import System.IO (hGetContents, hPutStr, stderr)
import System.Process (StdStream (CreatePipe), createProcess, shell, std_err, std_in, std_out, waitForProcess)
import Utils

runCmd :: Text -> Text -> IO (Text, Text, ExitCode)
runCmd cmdWithArgs textForProcess = do
  (Just rawHandle, Just hout, Just herr, jHandle) <-
    createProcess
      (shell $ toS cmdWithArgs)
        { std_out = CreatePipe,
          std_err = CreatePipe,
          std_in = CreatePipe
        }

  hPutStr rawHandle (toS textForProcess)
  out <- hGetContents hout <&> toS
  err <- hGetContents herr <&> toS

  exitCode <- waitForProcess jHandle
  return (out, err, exitCode)

parseTimeReport :: Text -> Either Text [MonthYearReport]
parseTimeReport x = eitherDecode (toS x) & mapLeft toS

getDataFromBrowser :: Ctx -> IO FromBrowser
getDataFromBrowser (Ctx config _db) = do
  let cmd = extractorCmd config & T.replace "%USER%" (user config) & T.replace "%URL%" (url config)
  putStrLn $ "extractorCmd = " <> show cmd
  (retOut, retErr, retCode) <- runCmd cmd (password config)
  when (retCode /= ExitSuccess) $ do
    hPutStr stderr $ "FAIL: Extractor returned " <> tshow retCode <> ".\n" <> retOut <> retErr & toS
    exitFailure
  let rawRes = retOut & parseTimeReport & forceEitherMsg "Failed to parse output from extractor command."
  return $ rawRes <&> \MonthYearReport{..} -> (tshow year <> "-" <> tshow month, hours)
