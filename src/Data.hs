{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}

module Data where

import Data.Aeson
import Data.Text (Text)
import qualified Data.Text as T
import GHC.Generics

data Config = Config
  { url :: Text,
    user :: Text,
    password :: Text,
    perHour :: Double,
    currency :: Text,
    workingDaysPerWeek :: Double,
    workDaysInMonthCmd :: Text,
    extractorCmd :: Text
  }
  deriving (Generic, Eq, Show)

instance FromJSON Config

data Leave = Leave
  { month :: Int,
    year :: Int,
    days :: Double
  }
  deriving (Generic, Eq, Show)

instance FromJSON Leave

data Sick = Sick
  { month :: Int,
    year :: Int,
    days :: Double
  }
  deriving (Generic, Eq, Show)

instance FromJSON Sick

data Extra = Extra
  { month :: Int,
    year :: Int,
    hours :: Double
  }
  deriving (Generic, Eq, Show)

instance FromJSON Extra

data Db = Db
  { leave :: [Leave]
  , sick :: [Sick]
  , extra :: [Extra]
  }
  deriving (Generic, Eq, Show)

instance FromJSON Db

data Ctx = Ctx Config Db

data MonthResult = MonthResult
  { monthString :: Text,
    month :: Int,
    year :: Int,
    hoursWorked :: Double,
    leaveDays :: Double,
    sickDays :: Double,
    extraHours :: Double,
    workDays :: Int,
    bonusHours :: Double,
    bonus :: Double,
    expectedHoursToWork :: Double
  }
  deriving (Generic, Eq, Show)

emptyMonthResult :: MonthResult
emptyMonthResult =
  MonthResult
    { monthString = "",
      month = 0,
      year = 0,
      hoursWorked = 0,
      leaveDays = 0,
      sickDays = 0,
      extraHours = 0,
      workDays = 0,
      bonus = 0,
      bonusHours = 0,
      expectedHoursToWork = 0
    }

type FromBrowser = [(Text, Double)]

data MonthYearReport = MonthYearReport
  { month :: Int,
    year :: Int,
    hours :: Double
  }
  deriving (Generic, Eq, Show)

instance FromJSON MonthYearReport
