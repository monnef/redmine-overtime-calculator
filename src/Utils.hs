module Utils
  ( (>>>),
    (<&>),
    (&),
    when,
    parseDoubleT,
    parseIntT,
    tshow,
    takeRight,
    Text,
    toS
  )
where

import Control.Arrow ((>>>))
import Control.Lens.Operators ((<&>))
import Data.Function ((&))
import Control.Monad (when)
import Data.Text (Text)
import Data.String.Conv (toS)

import qualified Data.Text as T

parseDoubleT :: Text -> Double
parseDoubleT = T.unpack >>> read

parseIntT :: Text -> Int
parseIntT = T.unpack >>> read

tshow :: Show a => a -> Text
tshow = show >>> T.pack

takeRight :: Int -> [a] -> [a]
takeRight n = reverse . take n . reverse
