{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE RecordWildCards #-}

module Main where

import Data.Conf.Json (readParse)
import Data.Either.Combinators (fromRight, fromRight')
import Data.Maybe (fromMaybe)
import System.Process (readCreateProcessWithExitCode, shell)
import Data.List (intercalate, find, groupBy)
import System.Exit (ExitCode(..), die)
import Text.Printf (printf)
import Data.Eq.HT (equating)
import Data.Time (getCurrentTime, formatTime, defaultTimeLocale, months)

import qualified Data.Text as T
import qualified Network.URI.Encode as URIEncode
import qualified Text.Tabular as Tabular
import qualified Text.Tabular.AsciiArt as TabularAscii

import Utils
import Data
import Constants

import RedmineExtractorBackend

lastNMonthsToProcess :: Int
lastNMonthsToProcess = 13

encodeUrl = URIEncode.encode

-- io = liftIO

hpmToMonthResult :: (T.Text, Double) -> MonthResult
hpmToMonthResult (monthString, hours) =
  emptyMonthResult { monthString = monthString
                   , month = month
                   , year = year
                   , hoursWorked = hours
                   }
  where [year, month] = T.splitOn "-" monthString <&> parseIntT


formatCurrency :: Config -> Double -> T.Text
formatCurrency c x = tshow x <> " " <> currency c


fillWorkDays :: Config -> MonthResult -> IO MonthResult
fillWorkDays c x = do
  let cmd = workDaysInMonthCmd c & subStrFromMr "%YEAR%" year & subStrFromMr "%MONTH%" month & T.unpack
  (exitCode, stdOut, stdErr) <- readCreateProcessWithExitCode (shell cmd) ""
  when (exitCode /= ExitSuccess) $ do
    putStrLn stdErr
    die $ "Failed to get working days in month, following command failed: " <> cmd
  return $ x { workDays = parseIntT (T.pack stdOut) }
  where subStrFromMr str accessor = T.replace str $ tshow $ accessor x


fillBonus :: Ctx -> MonthResult -> MonthResult
fillBonus (Ctx c _) x =
  x { bonusHours = bonusHours
    , bonus = perHour c * bonusHours
    , expectedHoursToWork = expectedHoursToWork
    }
  where
    bonusHours = hoursWorked x + extraHours x - expectedHoursToWork
    timeJob = workingDaysPerWeek c / fullTimeWorkingDaysPerWeek
    wd = workDays x & fromIntegral
    leaveD = leaveDays x
    sickD = sickDays x
    hoursPerWorkDayPerTimeJob = fullTimeHoursPerWorkDay * timeJob
    rawExpectedHoursToWork = wd * hoursPerWorkDayPerTimeJob
    rawLeaveHours = leaveD * hoursPerWorkDayPerTimeJob
    rawSickHours = sickD * hoursPerWorkDayPerTimeJob
    expectedHoursToWork = rawExpectedHoursToWork - rawLeaveHours - rawSickHours
    fullTimeHoursPerWorkDay = 8.0
    fullTimeWorkingDaysPerWeek = 5

fillLeave :: Ctx -> MonthResult -> MonthResult
fillLeave (Ctx _ db) x = x { leaveDays = fromMaybe 0 ld } where
  ld = find f (leave db) <&> (days :: Leave -> Double)
  f :: Leave -> Bool
  f a = (month :: MonthResult -> Int) x == (month :: Leave -> Int) a
     && (year :: MonthResult -> Int) x == (year :: Leave -> Int) a

fillSick :: Ctx -> MonthResult -> MonthResult
fillSick (Ctx _ db) x = x { sickDays = fromMaybe 0 sd } where
  sd = find f (sick db) <&> (days :: Sick -> Double)
  f :: Sick -> Bool
  f a = (month :: MonthResult -> Int) x == (month :: Sick -> Int) a
     && (year :: MonthResult -> Int) x == (year :: Sick -> Int) a

fillExtra :: Ctx -> MonthResult -> MonthResult
fillExtra (Ctx _ db) x = x { extraHours = fromMaybe 0 eh } where
  eh = find f (extra db) <&> (hours :: Extra -> Double)
  f :: Extra -> Bool
  f a = (month :: MonthResult -> Int) x == (month :: Extra -> Int) a
     && (year :: MonthResult -> Int) x == (year :: Extra -> Int) a

genResult :: [MonthResult] -> Ctx -> String
genResult r (Ctx c _) = header : (r <&> p) & intercalate "\n"
  where
    header :: String
    header = intercalate sep ["monthYear", "year", "month", "workDays", "leaveDays", "sickDays", "extraHours", "expectedHoursToWork", "hoursWorked", "bonusHours", "bonus"]
    p :: MonthResult -> String
    p r = intercalate sep row where
      fd acc = printf "%.2f" $ acc r
      fi acc = show $ acc r
      fdc acc = fd acc <> " " <> (c & currency & T.unpack)
      fdh acc = fd acc <> "h"
      fdd acc = fd acc <> "d"
      fMonthYear year month = (months defaultTimeLocale !! (month - 1) & fst) <> " " <> show year
      row = [fMonthYear ((year :: MonthResult -> Int) r) ((month :: MonthResult -> Int) r), fi year, fi month, fi workDays, fd leaveDays, fd sickDays, fdh extraHours, fdh expectedHoursToWork, fdh hoursWorked, fdh bonusHours, fdc bonus]
      --    [fMonthYear year month, fi workDays, fd leaveDays, fd sickDays, fd extraHours, fdh expectedHoursToWork, fdh hoursWorked, fdh bonusHours, fdc bonus]
    sep = ","


process :: Ctx -> IO [MonthResult]
process ctx@(Ctx config db) = do
  hoursPerMonth <- getDataFromBrowser ctx
  let filledHpmAndLeave = hoursPerMonth & takeRight lastNMonthsToProcess <&> hpmToMonthResult <&> fillLeave ctx <&> fillSick ctx <&> fillExtra ctx
  putStrLn "Filling work days count using external command..."
  filledWorkDays <- mapM (fillWorkDays config) filledHpmAndLeave
  putStrLn "Computing bonuses..."
  let filledBonus = filledWorkDays <&> fillBonus ctx
  let result = filledBonus :: [MonthResult]
  putStrLn "Done."
  return result

printConfig :: Config -> IO ()
printConfig config = do
  let configMsg = "Config:\n"
                <>  "  url = " <> url config <> "\n"
                <>  "  user = " <> user config <> "\n"
                <>  "  perHour = " <> tshow (perHour config) <> " " <> currency config <> "\n"
                <>  "  currency = " <> currency config <> "\n"
                <>  "  workingDaysPerWeek = " <> tshow (workingDaysPerWeek config) <> "\n"
                <>  "  workDaysInMonthCmd = " <> tshow (workDaysInMonthCmd config) <> "\n"
                <>  "  extractorCmd = " <> tshow (extractorCmd config) <> "\n"
  putStr $ T.unpack configMsg


printDb :: Db  -> IO ()
printDb db = do
  let leaveStrs = map leaveToStr (leave db)
  let sickToStrs = map sickToStr (sick db)
  let msg = "DB:\n"
          <>  "  leave = " <> T.intercalate ", " leaveStrs <> "\n"
          <>  "  sick = " <> T.intercalate ", " sickToStrs <> "\n"
  putStr $ T.unpack msg
  where
  leaveToStr :: Leave -> T.Text
  leaveToStr l = tshow ((month :: Leave -> Int) l) <> "-" <> tshow ((year :: Leave -> Int) l) <> " = " <> tshow ((days :: Leave -> Double) l) <> "d"
  sickToStr :: Sick -> T.Text
  sickToStr l = tshow ((month :: Sick -> Int) l) <> "-" <> tshow ((year :: Sick -> Int) l) <> " = " <> tshow ((days :: Sick -> Double) l) <> "d"

parseConfig :: IO Config
parseConfig = readParse configFileName <&> fromRight'

-- TODO: should signal missing/unparsable DB, probably via Maybe?
parseDb :: IO Db
parseDb = readParse dbFileName <&> fromRight (Db { leave = [], sick = [], extra = [] })

printTable :: [MonthResult] -> Ctx -> IO ()
printTable result (Ctx c _) = do
  let columnHeaders =
        Tabular.Group
          Tabular.DoubleLine
          [
            Tabular.Group
              Tabular.SingleLine
              [ Tabular.Header "Full month"
              ],
            Tabular.Group
              Tabular.SingleLine
              [ Tabular.Header "Work days",
                Tabular.Header "Leave days",
                Tabular.Header "Sick days",
                Tabular.Header "Extra hours"
              ],
            Tabular.Group
              Tabular.SingleLine
              [ Tabular.Header "Expected hours",
                Tabular.Header "Hours worked"
              ],
            Tabular.Group
              Tabular.SingleLine
              [ Tabular.Header "Bonus hours"
              , Tabular.Header "Bonus"
              ]
          ]
  let monthResToDateStr = \MonthResult {year = y, month = m} ->
        let mStr = show m
            mStrPad = if length mStr == 1 then " " else ""
         in show y <> "-" <> mStr <> mStrPad
  let dateGroups = result & groupBy (equating (year :: MonthResult -> Int)) <&> fmap monthResToDateStr
  let dateGroupToHeaders = \dateGroup ->
        Tabular.Group
          Tabular.NoLine
          (dateGroup <&> Tabular.Header)
  let rowHeaders =
        Tabular.Group
          Tabular.SingleLine
          (dateGroups <&> dateGroupToHeaders)
  let tableData =
        result <&> \MonthResult {..} ->
          [fMonthYear year month, fi workDays, fd leaveDays, fd sickDays, fd extraHours, fdh expectedHoursToWork, fdh hoursWorked, fdh bonusHours, fdc bonus]
  let table = Tabular.Table rowHeaders columnHeaders tableData
  putStr $ TabularAscii.render id id id table
  where
    fd = printf "%.2f"
    fi = show
    fdc x = fd x <> " " <> (c & currency & T.unpack)
    fdh x = fd x <> " h"
    fdd x = fd x <> " d"
    fMonthYear year month = (months defaultTimeLocale !! (month - 1) & fst) <> " " <> show year

main :: IO ()
main = do
  putStrLn "Redmine Overtime Calculator by monnef"

  config <- parseConfig
  printConfig config

  db <- parseDb
  printDb db

  let ctx = Ctx config db
  result <- process ctx
  let resultString = genResult result ctx
  putStrLn $ "\nResults:\n\n" <> resultString
  putStrLn "" >> printTable result ctx
  dateStr <- getCurrentTime <&> formatTime defaultTimeLocale "%0Y-%m-%d_%H-%M-%S"
  let resultFileName = "result_" <> dateStr <> ".csv"
  writeFile resultFileName resultString
  putStrLn $ "\nSaved result to \"" <> resultFileName <> "\"."
