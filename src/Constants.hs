{-# LANGUAGE OverloadedStrings #-}

module Constants where

import Data.Text (Text)

configFileName :: FilePath
configFileName = "config.json"

dbFileName :: FilePath
dbFileName = "db.json"

closeBrowser :: Bool
closeBrowser = True

reportUrlSuffix1 :: Text
reportUrlSuffix1 = "/time_entries/report?"

-- utf8=✓&f[]=spent_on&op[spent_on]=*&f[]=user_id&op[user_id]=%3D&v[user_id][]=me&f[]=&columns=month&criteria[]=project

reportUrlSuffix2 :: Text
reportUrlSuffix2 = "f%5B%5D=spent_on&op%5Bspent_on%5D=%2A&f%5B%5D=user_id&op%5Buser_id%5D=%3D&v%5Buser_id%5D%5B%5D=me&f%5B%5D=&columns=month&criteria%5B%5D=project"
